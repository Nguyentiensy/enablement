### Email Format 

Another month another amazing GitLab release.   

GitLab is continuing to invest in a COMPLETE DevOps Platform for the entire software development lifecycle that helps our customers:
* Increase operational efficiencies
* Deliver better products faster
* Reduce risk and security compliance

**ADD** 3-4 updates from GitLab. Usually content (blog, remote work, etc.) or big announcements (i.e. partner program launch)

Please contact us if you would like to schedule time with a Solution Architect to review GitLab **VERSION NUMBER**, Upcoming Releases, or the **NEXT VERSION NUMBER** Kickoff. 

**ADD** Copy/paste release information from the [GitLab Releases page](https://about.gitlab.com/releases/) below. 




----
## Process
1. Follow #release-post channel closely and use information from the [GitLab Releases page](https://about.gitlab.com/releases/) to complete email once posted. 
1. Copy email into a template in Outreach. 
   1. Store it under the "Sales 2020 GitLab Feature Releases" campaign. 
   1. Make sure you set sharing settings to, "Others can see and use it." 
1. Send sales a notification in #sales Slack channel:
   1. The release post is live (share link to blog) 
   1. Check your email for a version you can easily forward to customers, or it's [here] (link to template) in Outreach for anyone who wants to access it there. 
   1. Remember to share the release post on social.
